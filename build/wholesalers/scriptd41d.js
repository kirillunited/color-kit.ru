function toggleOpt(el){
    if (document.getElementById('formopt_div').style.display == 'none'){
        document.getElementById('formopt_div').style.display = 'block';
        el.setAttribute('data-bool', 1);
    } else {
        document.getElementById('formopt_div').style.display = 'none';
        el.setAttribute('data-bool', 0);
    }
    return false;
}
function sendOpt(el){
    var name = document.getElementsByName('name')[0];
    if (name.value.replace(/\s/g, '').length == 0){
        alert('Заполните поле "Имя"');
        name.focus();
        return false;
    }
    var last_name = document.getElementsByName('last_name')[0];
    if (last_name.value.replace(/\s/g, '').length == 0){
        alert('Заполните поле "Фамилия"');
        last_name.focus();
        return false;
    }
	var second_name = document.getElementsByName('second_name')[0];
    if (second_name.value.replace(/\s/g, '').length == 0){
        alert('Заполните поле "Отчество"');
        second_name.focus();
        return false;
    }
    var phone_code = document.getElementsByName('phone_code')[0];
    if (phone_code.value.replace(/[^0-9]/g, '').length == 0){
        alert('Заполните поле "Телефон"');
        phone_code.focus();
        return false;
    }
    var phone = document.getElementsByName('phone')[0];
    if (phone.value.replace(/[^0-9]/g, '').length == 0){
        alert('Заполните поле "Телефон"');
        phone.focus();
        return false;
    }
    var email = document.getElementsByName('email')[0];
    if (email.value.replace(/[^@]/g, '').length == 0){
        alert('Заполните поле "E-mail"');
        email.focus();
        return false;
    }
	var city = document.getElementsByName('city')[0];
    if (city.value.replace(/\s/g, '').length == 0){
        alert('Заполните поле "Город"');
        city.focus();
        return false;
    }
    var password = document.getElementsByName('password')[0];
    if (password.value.length == 0){
        alert('Заполните поле "Пароль"');
        password.focus();
        return false;
    }
    var password2 = document.getElementsByName('password2')[0];
    if (password2.value.length == 0){
        alert('Подтвердите пароль');
        password2.focus();
        return false;
    }
    if (password.value != password2.value){
        alert('Введеные пароли не совпадают');
        password2.focus();
        return false;
    }
	var topic = document.getElementsByName('topic')[0];
	var FIELD_TYPE_1 = $("#FIELD_TYPE_1 option:selected").val();
	var FIELD_TYPE_2 = $("#FIELD_TYPE_2 option:selected").val();
    var dispatch = $("[name=dispatch]").val();
    //var form = $("#formopt_div #formopt").serialize();
    el.disabled = true;	
	//alert(topic.value);
    //$.post("/wholesalers/res.php", form, function(data){
	$.ajax({
      type: "POST",
      url: "/wholesalers/res.php",
      data: 'name='+name.value+'&last_name='+last_name.value+'&second_name='+second_name.value+'&phone_code='+phone_code.value+'&phone='+phone.value+'&email='+email.value+'&city='+city.value+'&password='+password.value+'&password2='+password2.value+'&topic='+topic.value+'&FIELD_TYPE_1='+FIELD_TYPE_1+'&FIELD_TYPE_2='+FIELD_TYPE_2+'&dispatch='+dispatch,
	  success: function(html){
        if (html == 'OK'){
            var link = document.getElementById('toggle-opt-form');
            toggleOpt(link);
            var div = document.createElement('div');
            div.innerHTML = '<p style="font-weight: bold;margin: 10px 0 0 0;">Спасибо, Ваша заявка отправлена. В ближайшее время с Вами свяжется менеджер для уточнения деталей сотрудничества.</p>';
            link.parentNode.insertBefore(div, link.nextSibling);
            document.getElementsByName('name')[0].value = '';
            document.getElementsByName('last_name')[0].value = '';
			document.getElementsByName('second_name')[0].value = '';
            document.getElementsByName('phone_code')[0].value = '';
            document.getElementsByName('phone')[0].value = '';
            document.getElementsByName('email')[0].value = '';
            document.getElementsByName('password')[0].value = '';
            document.getElementsByName('password2')[0].value = '';
            document.getElementsByName('city')[0].value = '';
            document.getElementsByName('topic')[0].value = '';
        }
        else {
            alert(html);
        }
        el.disabled = false;
    }
	});
    
    return false;
}